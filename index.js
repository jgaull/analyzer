

const express = require('express')
const app = express()

app.get('/', (req, res) => {
    res.status(200).send('ok')
})

const server = app.listen(process.env.PORT || 9000, () => {
    const port = server.address().port
    console.log('Example app listening at port %s', port)
})

module.exports = server